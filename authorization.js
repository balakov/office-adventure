const SESSION_SECRET = process.env.SESSION_SECRET
const DATABASE = process.env.DATABASE_URL
const LOGIN_REQUIRED = false;
var tte = require('./bin/tinytextengine.js'); 


module.exports.doLogin = 
    function(req,res,next){
    
    console.log('middleware!' + SESSION_SECRET);

 if(!req.session.user){
        req.session.user = {};
        req.session.user.player = tte.createPlayer();
        req.session.user.auth = { state : 'nologin'};
        console.log("New Player connected")
        //res.send('You are not logged in. Type l(ogin) or r(egister) as a new User')
        //return;
    } 
    if (LOGIN_REQUIRED || req.query.msg == 'login'){
        if(req.session.user.auth.state != 'loggedin'){
            guideLogin(res, req.session.user, req.query.msg )
            return;
        }                 
    }        
    if (req.query.msg == 'logout'){
        if(req.session.user.auth.state != 'loggedin'){
            res.send("Cannot perform logout: You are not logged in")
        } else {
            savePlayerData(req.session.user, res);
            delete req.session.user;
            
            return;
        }
    }
    next()
}


const { Client } = require('pg');
const client = new Client({
  connectionString: DATABASE,
  ssl: true,
});
console.log("Connecting to: " + DATABASE) 
client.connect();

var verifyPassword = function(userpw, storedpw, salt){
    const hash = crypto.createHash('sha256'); 
    var hpw = hash.update(userpw + salt);
    return (hpw.digest('hex') == storedpw)
}

var insertUser = function (user, res){
    console.log("creating user " + user.auth.name + " with password " + user.auth.pw + " and data: " + util.inspect(user.player,false, null));
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto.createHash('sha256'); 
    hash.update(user.auth.pw + salt);
    var encryptedpw = hash.digest('hex');
    const params = [user.auth.name, encryptedpw, user.player, salt]
    var qr = client.query("INSERT INTO users (playername,pw,data,salt) VALUES ($1::text,$2::text,$3::jsonb,$4::text);",params);
    qr.then(function(result){
         console.log("User created")
         user.auth.state = 'loggedin';
         res.send("user created'");
         return;     
    }).catch(e => console.error(e.stack))   
}

var loginUser = function(user, res){
    const params = [user.auth.name];
    var qr = client.query("SELECT * FROM users WHERE playername=$1::text",params);
    qr.then(function(result){
         if(result.rowCount == 1){
            console.log("correct pw " + result.rows[0].pw + "userpw: " + user.auth.pw) 
            if(verifyPassword(user.auth.pw, result.rows[0].pw, result.rows[0].salt)){
                console.log("Login successful!")
                user.auth.state = 'loggedin';
                console.log("loading user " + user.auth.name +" with data " + util.inspect(result.rows[0].data))
                user.player = result.rows[0].data;
                res.send({text: "SUCCESS", prompt: user.auth.name + VHOST});
            } else {
                 console.log("wrong pw");
                 user.auth.state = 'nologin'
                 res.send("LOGIN FAILED. Wrong password. Type l(ogin) or r(egister) as a new User'");
                 return;   
            }        
         } else {
             console.log("No such user");
            user.auth.state = 'nologin'
            res.send("LOGIN FAILED. No such user. Type l(ogin) or r(egister) as a new User'");
            return;             
         }           
    }).catch(e => console.error(e.stack))
}
var savePlayerData = function(user, res){
    console.log("saving " +user.auth.name + " with data: " + util.inspect(user.player, false, null));
    const params = [user.player,user.auth.name]
    var qr = client.query("UPDATE users SET data=$1::jsonb WHERE playername = $2::text;",params);
    qr.then(function(result){
         console.log("progress saved");
         res.send("Progress saved");
         return;     
    }).catch(e => console.error(e.stack))       
}

var guideLogin = function(res, user, input){
        console.log("Guiding login, state: " + user.auth.state)
        if(user.auth.state == 'enter_pw'){
            user.auth.pw = input;
            console.log("logging in with " + user.auth.name + " pw: " + user.auth.pw)
            loginUser(user,res);
            return;
                
        } else if (user.auth.state == 'enter_name'){
            user.auth.name = input;
            user.auth.state = 'enter_pw';
            res.send("Enter Password:")
        }else if (user.auth.state == 'enter_new_pw'){
            user.auth.pw = input;
            user.auth.state = 'loggedin';
            insertUser(user, res);
            return;
        } else if (user.auth.state == 'enter_new_username'){
            user.auth.name = input;
            user.auth.state = 'enter_new_pw';
            res.send("Create Password:")

        }else if(input == 'login'|| input == 'l'){
            user.auth.state = 'enter_name';
            res.send("Enter name:")
        } else if(input == 'register' || input == 'r'){
            user.auth.state = 'enter_new_username';
            res.send("Enter new Username:")
        } else {
            res.send('You are not logged in. Type l(ogin) or r(egister) as a new User')
        } 
}

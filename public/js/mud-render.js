var mudrender = (function(){
    var root;
    var prompt;
    var cmdline;
    
    var userInputCallback = function(){console.log("No UserInputCallback defined :(")};
    var queue = [];

    var noEcho = false;
    
    var skipRendering = false;   
    var inProgress = false;
    var skipListener = function(e){
        if(e.keyCode == 13){
                console.log("SKIPPING");
                skipRendering = true;
        }

    }
    
    var lastLine;
    
    var rainbowtable = ['255,0,0', '255,127,0','255,255,0','127,255,0','0,255,0','0,255,255','0,127,255','0,0,255','127,0,255','255,0,127'];
    var rainbowidx =0;
    
    function init(){
        // input line
        cmdline = document.createElement("input"); 
        cmdline.setAttribute('type',"text");
        cmdline.setAttribute('id',"cmdline")
        cmdline.setAttribute('autocomplete',"off")
        cmdline.addEventListener("keyup", function(e){
            if(e.keyCode == 13){
                collectInput(cmdline.value);
                e.stopPropagation();
            }
            
        });
       
        prompt = document.createElement("div");  
        prompt.setAttribute('id',"inputPrompt")
        prompt.textContent = 'anonymous@oa>';
        prompt.appendChild(cmdline);
        


        root.appendChild(prompt);
        root.setAttribute('class', "mud-console");

        
    }
    
    function collectInput(inputText){
        console.log("User typed: " + inputText);
        // don't echo when input was hidden
        if(!noEcho){
            var lastline = document.getElementById('inputPrompt').innerText + inputText;
            console.log("lastline: " + lastline);
            var div = document.createElement('div');
            div.innerHTML = lastline
            root.insertBefore(div, document.getElementById('inputPrompt')); 
        }
            
        userInputCallback(inputText);
        
        cmdline.value = '';
    }
    
    function renderDeferred(text, line, speed, callback){
        inProgress = true;

        if(text.length>0){
            if(skipRendering || !speed){
                line.innerHTML = line.innerHTML + text;
                skipRendering = false;
                inProgress = false;
                if(callback){
                    console.log("skipped deferred rendering")
                    callback();
                }
                return;
            }
            line.innerHTML = line.innerHTML + text.charAt(0);        
            setTimeout(function(){renderDeferred(text.slice(1,text.length), line, speed, callback)}, speed);
        } else {
            skipRendering = false;
            inProgress = false;
            if(callback){
                console.log("finished deferred rendering")
                callback();
            }
            return;
        }          
            
        
    }
    function rainbow(element, idx){
        if(!idx){
            var txt = element.innerHTML;
            element.innerHTML = '';
            console.log(txt);
            for(var i=0; i < txt.length; i++){
                rainbowidx%=rainbowtable.length;
                var l = document.createElement("span");
                l.style.color = "rgb("+rainbowtable[rainbowidx++]+")";
                l.innerHTML = txt.charAt(i);
                element.appendChild(l);
            }  
            setTimeout(function(){rainbow(element, 1)},200);
            
            
        } else {
            for(var i=0; i < element.childNodes.length; i++){
                rainbowidx%=rainbowtable.length;
                var l = element.childNodes[i];
                l.style.color = "rgb("+rainbowtable[rainbowidx++]+")";
               
            }  
            setTimeout(function(){rainbow(element, 1)},200);
            
        }

        
        
        
    }
    
    function unhideInput(){
        //inputContainer.style.display = 'block';
    }
    function hideInput(){
        //inputContainer.style.display = 'none';
    }
    
    
    return {
        create: function(docElement, userInputCallbackFunc, bufferSize){
            root = docElement;
            userInputCallback = userInputCallbackFunc;
            init();
            console.log("created");
        },
        
        renderSimple: function(text){
            var div = document.createElement('div');
            var x = document.createElement("br");
            div.appendChild(x);
            div.innerHTML = text;
            root.insertBefore(div, prompt);   
                document.getElementById("cmdline").focus(); 
        },
        render : function(msg){
            console.log("RENDERING: " + msg);
            // compute target line
            var div = document.createElement('div');
            root.insertBefore(div, document.getElementById('inputPrompt'));
            targetLine = div; 

            
            // if allowed, add ways for the user to skip message
            if(msg.unskippable){
                console.log("unskippable")
                document.body.removeEventListener("keyup", skipListener);
            } else {
                console.log("skippable")
                document.body.addEventListener("keyup", skipListener);
            }
            // if it is to be replaced, clear it.
            if(msg.replace){
                targetLine.innerHTML = "";
            }
            
            if(msg.color){
                targetLine.style.color = msg.color;
            }
            if(msg.bgcolor){
                targetLine.style.backgroundColor = msg.bgcolor;
            }
            
            
            // does the user receive a prompt immediately?
            if(!msg.async){
                console.log("non-async");
                hideInput();
                renderDeferred(msg.text, targetLine, msg.speed, unhideInput);                 
            } else {
                renderDeferred(msg.text, targetLine, msg.speed); 
            }

            if(msg.effect == "super-gay"){
                console.log("rainbowing ", targetLine)
                rainbow(targetLine);
            }

            if(msg.hideNextInput){
                cmdline.setAttribute("type", "password");
            } else {
                cmdline.removeAttribute("type", "password");
            }
            
            if(msg.prompt){
                console.log("changing prompt to " + msg.prompt)
                var tmp = document.getElementById('cmdline')
                prompt.innerHTML = msg.prompt;
                prompt.appendChild(tmp);
            }
            lastLine = targetLine;
            document.getElementById("cmdline").focus(); 
                              
        }
        
    }

}());
const assert = require('chai').assert;
var rewire = require("rewire");
var extfile = require("../bin/roomdata.js");

const engine = rewire('../bin/tinytextengine.js');
let Entity = engine.__get__('Entity')
let Door = engine.__get__('Door')
let Room = engine.__get__('Room')


// generic entities
let window = new Entity("window", "a large window", ()=>"The window covers the whole wall. ")
let desk = new Entity("desk", "a desk", ()=>"Looks like standard office equipment, nothing out of the ordinary. ")
let paperbin = new Entity("bin", "a paper bin", ()=>"Inside are used teabags, covered by some papers. ")
let blank_wall = new Entity("wall", "a blank wall", ()=>"You stare at the wall, with a blank expression. ") 

// unique entities
let office_hallway_door = new Door('office','hallway')
let hallway_kitchen_door = new Door('kitchen','hallway')
let crack = new Entity("crack","a cracked wall", ()=>"The fine crack runs from top to bottom. You wonder if it is widening.")


describe('Persistence ', function() {    
    it('Loading from JSON', function() {
        let win = extfile.data.entities[0]
        let winEntity=engine.createEntityFromData(win)
        console.log(winEntity.transitions[0].action)
    });
        
        /*
        let testroom = engine.__get__('allRooms')['office']//new Room('kitchen',[blank_wall],[hallway_kitchen_door],[blank_wall],[blank_wall],()=>"A small kitchen")
        console.log("**testroom: " ,testroom)
        let flattened = engine.serializeRoom(testroom)
        console.log("**serialized: " ,flattened)
        let inflated = engine.deserializeRoom(flattened)
        console.log("**deserialized: " ,inflated)
        
      //assert.equal(result, 'foo' ) 
   });
  /* it('testcase', function() {
      let player = _925.createNewPlayer();
       tte.init(_925.rooms, _925.createNewPlayer)
       //let room = tte.getRoom('personaloffice');
      // console.log(room.items)
       
      //assert.equal(result, 'foo' ) 
   }); */
    
    
});
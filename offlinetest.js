const readline = require('readline');
const engine = require('./bin/tinytextengine.js')

let p1 = engine.createPlayer();

var rl = readline.createInterface(process.stdin, process.stdout);
rl.setPrompt('cmd> ');
rl.prompt();
rl.on('line', function(line) {
    engine.applyInput(p1, line);
    console.log(p1.output)
    p1.output = '';
    rl.prompt();

}).on('close',function(){
    process.exit(0);
});

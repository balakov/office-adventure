var AP = require('./adventurousparser.js');
let Room = require('./tinytextengine.js').Room
let Entity = require('./tinytextengine.js').Entity
let Transition = require('./tinytextengine.js').Transition
let Door = require('./tinytextengine.js').Door

let api = require('./tinytextengine.js').API

module.exports.getRooms = function(){
    // generic entities
    let desk = new Entity("desk", "a desk", ()=>"Looks like standard office equipment, nothing out of the ordinary. ")
    let paperbin = new Entity("bin", "a paper bin", ()=>"Inside are used teabags, covered by some papers. ")
    let blank_wall = new Entity("wall", "a blank wall", ()=>"You stare at the wall, with a blank expression. ") 

    // unique entities
    let office_hallway_door = new Door('office','hallway')
    let hallway_kitchen_door = new Door('kitchen','hallway')
    let crack = new Entity("crack","a cracked wall", ()=>"The fine crack runs from top to bottom. You wonder if it is widening.")
    
    
    let useblinds = new Transition( new AP(":use blinds"), function(player, params, self) {
        console.log('self', self)
        console.log("OHAI!!!" + self.tag)
        self.state = self.state || {}
        if(params.use == 'open')
            self.state.blinds = 'open'
        else self.state.blinds = 'closed'
        player.output += 'You '+params.use+' the blinds'
    })
    
    
    let window = new Entity("window", "a large window", ()=>"The window covers the whole wall. ", [useblinds])

    // custom transitions


    // rooms

    let describe = ()=>" A dim lighted office with " +office.state.blinds + " blinds"
    let office = new Room('office',[office_hallway_door],[crack],[desk, paperbin],[window], describe, {blinds: 'closed'})
   // office.transitions.push(useblinds)



    let allRooms = {
        office : office,
        hallway : new Room('hallway',[blank_wall],[blank_wall],[office_hallway_door],[hallway_kitchen_door],()=>" A small hallway"),
        kitchen : new Room('kitchen',[blank_wall],[hallway_kitchen_door],[blank_wall],[blank_wall],()=>"A small kitchen")
    } 
    return allRooms
}
    
    
    
    


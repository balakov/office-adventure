const util = require('util')

var rooms = {
    personaloffice : {
		commandParsers: [
			[
			"open [the] blind[s]",
        	function(p) { 
			p.personaloffice._state.blinds = 'open';
			return "The blinds are now open. They are now watching you.."}
			],
			[
			"close [the] blind[s]",
        	function(p) { 
			p.personaloffice._state.blinds = 'closed';
			return "The blinds are now closed. But they have seen you."}
			],
            [
			"look [at|the] [window|outside]",
        	function(p) { 
			     if(p.personaloffice._state.blinds == 'closed'){
                     return "You peek between the shutter blades. The sun hurts your eyes."
                 } else {
                     return "You see an inner yard that is enclosed by the tall office building from all sides."
                 }
            }
			],
			[
			"(go|use door)",
        	function(p) { 
            console.log("player is " + util.inspect(p, false, null));   
                p.currentRoom ='smallhallway';
                p.facing = 'north';
                p.smallhallway._state.timesVisited+=1;
                }
			],
			[
			"(take) :item",
        	function(p, v) {
				var idx = p.personaloffice._state.items.indexOf(v.item);
				if(idx!=-1){
					p.inventory.push(v.item);
					return "You took " +v.item
				} else return "What " + v.item +"?"; 
			
			}
			]
		],
        description: {
            full: function(p){return "A small room, with " + p.personaloffice._state.blinds + " blinds."},
            north : function(){return "a door"},
            east : function(){return "a cracked wall"},
            south : function(){return "nothing"},
            west : function(){return "a window"}
        },
		defaultState: {
			timesVisited: 0,
			blinds: 'closed',
			door: 'closed', 
			items: ['pen']
		}
      },
    communitykitchen : {
		commandParsers: [
			[
			"(go|use door)",
        	function(p) { 
            p.facing = 'east';    
			p.currentRoom ='smallhallway';
			return "You enter the hallway"}
			],
            [
			"listen",
        	function(p) { 
                return rooms.communitykitchen.globalState.test
            }
			],
            [
			"say :msg",
        	function(p, params) { 
                rooms.communitykitchen.globalState.test = params.msg;
                return "You say " + params.msg;
            }
			]
		],
        description: {
            full: function(){return "A community place. The air smells of stale coffee"},
            west : function(){return "a large glass front"},
            north : function(){return "a kitchen counter and a sink"},
            east : function(){return "a door to the hallway and a another door"},
            south : function(){return "an old cigarette vending machine"},
        },
		defaultState: {
			timesVisited: 0,
			blinds: 'closed',
			door: 'closed', 
			items: ['pen'],
			entities: ['coffeemachine']
		}, globalState: {
			test : 'empty'
		}
      },
    smallhallway: {
		commandParsers: [
			[
			"(go|use door)",
        	function(p) {
                if(p.facing=='south'){
                   p.currentRoom ='personaloffice'; 
                } else if(p.facing == 'west'){
                   p.currentRoom ='communitykitchen'; 
                } else if(p.facing == 'east'){
                   return "the door is locked. You are sure this in breach of fire safety regulations"
                } else return "Which door?"  
			return "You go through the door"}
			]
		],
        description: {
            full: function(){return "a featureless small hallway"},
            north : function(){return "a 'no smoking' sign"},
            east : function(){return "a closed door"},
            south : function(){return "the door to your personal office"},
            west : function(){return "an open door"},
        },
		defaultState: {
			timesVisited: 0			
		}
      }

  
}

var entities = {
	coffeemachine : {
		
	}
} 

var triggers = 
    [
       function (player){
           if(player[player.currentRoom]._state.timesVisited == 0){
               return "You enter this room for the first time.";
           } else return "";
       },
        function (player){
           if(player.currentRoom == 'smallhallway' && player.inventory.length == 0){
               return "You enter the hallway empty-handed.";
           } else return "";
       }
    ]


var createNewPlayer = function(){
        
 		return {
			name: 'guy',
			currentRoom: 'personaloffice', 
            facing: 'north',
			inventory: [],
			personaloffice: {
				_state : rooms.personaloffice.defaultState
			},
			communitykitchen: {
				_state : rooms.communitykitchen.defaultState
			},
            smallhallway: {
				_state : rooms.smallhallway.defaultState
			}
		}           
}

module.exports.rooms = rooms
module.exports.triggers = triggers
module.exports.createNewPlayer = createNewPlayer  


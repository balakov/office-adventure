var AP = require('./adventurousparser.js');
const getDir = require('./roomutils').turn
const getDirectionId =require('./roomutils').getDirectionId;
const makeReadableList =require('./roomutils').makeReadableList;


let Transition = function(parser, action){
    this.parser = parser
    this.action = action
}



let Quest = function(trigger, action){
    let quest = {}
    quest.trigger = trigger
    quest.action = action
    return quest
}

let EnterRoomFirstTimeQuest = function(roomName, phrase){
    q = new Quest((player)=>{
    if(player.roomdata[roomName] &&player.roomdata[roomName].timesVisited == 1)
        return true
    else return false    
    }, function(player){
        player.output += phrase
        q.state.resolved = true;
    })
    return q
}



let Room = function(id, north, east, south, west, describe, state){
        
    let room = {}
    room.state = state || {}
    room.id = id
    room.state.timesVisited =0
    room.getEntities = function(){        
        return room.directions.reduce((acc, curr)=>{
            //console.log('Entity in room: ', curr );
            return acc.concat(curr)},[])    
    }
    room.getEntityTransitions = function(){
        let transitions = room.getEntities().reduce((acc, curr)=>{
                //console.log('Adding transition ', curr.transitions)
                return acc.concat(curr.transitions)},[])
        //console.log('Entity transitions: ',transitions)
        return transitions
    }
    room.describeDirection = function(dir){
        let identifiers = room.directions[getDirectionId(dir)].map(x=>x.identifier)
        return makeReadableList(identifiers)
    }
    room.describeFully = function(player){
        return ' Ahead of you is ' + room.describeDirection(player.facing) +
            '. To your left is ' + room.describeDirection(getDir(player.facing, 'left')) +
            '. To your right is ' + room.describeDirection(getDir(player.facing, 'right')) +  
            '. Behind you is ' + room.describeDirection(getDir(player.facing, 'back'))     
        
    } 
    let turn = new Transition( new AP("turn :dir"), function(player, params) {
        let dir = getDir(player.facing, params.dir)
        if(dir){
            player.facing = dir
            player.output += 'Ahead of you is ' + room.describeDirection(dir);} else { player.output += ' You spin wildly.'}
        
        }
    )
    let look= new Transition( new AP("look"), function(player, params) {
        player.output = room.describe() + room.describeFully(player);
        }
    ) 
    let lookEntity= new Transition( new AP("look :entity"), function(player, params) {
        let entity = room.getEntities(player).filter(x=>x.tag == params.entity)
        if(entity.length ==1)
            player.output = entity[0].describe();
        else if(entity.length ==0)
            player.output = "No such thing as a " + params.entity + " here";
        else if(entity.length >1)
            player.output = "In this room is more than one " + params.entity
        }
    )
    let say= new Transition( new AP("say :msg"), function(player, params) {
        player.output = "You say " + params.msg;
        player.broadcast = "Player says: " + params.msg
        }
    ) 
    room.transitions = [turn, look, lookEntity, say]
    room.directions=[]
    room.directions.splice(0,0,north,east,south,west)
    room.describe = describe
    room.getTransitionsInDirection = (dir)=>{
        let t = room.directions[getDirectionId(dir)].reduce((acc, curr)=>{
            //console.log('Entity in room: ', curr );
            return acc.concat(curr.transitions)},room.transitions) 
        //console.log('transitions in direction of player: ' +dir , t )
        return t;
    } 
    room.getEntitiesInDirection = (dir)=>{
        let t = room.directions[getDirectionId(dir)]
        return t;
    } 
    
    room.getAllTransitions = ()=>{
        let t = room.getEntityTransitions().concat(room.transitions)
        //console.log('All transitions in room: ',t)
        return t
        }
    room.getTransitions = ()=> room.transitions || []
    return room    
}

// tag: command keyword, identifier=short description, describe=describing function, transitions

let Entity = function(tag, identifier, describe, transitions){
    let e = {}
    e.describe = describe
    e.identifier = identifier
    e.tag = tag
    e.transitions = transitions || []
    e.isApplicable = function(input){
        return e.transitions.filter(t=>t.parser.match(input).matching).length>0
    }
    e.applyInput = function(player, input){
        e.transitions.filter(t=>{console.log('checking t' + t); return t.parser.match(input).matching})
        .map(t=>{console.log('matching t: '+t);t.action(player, t.parser.match(input).params, e)})
    }
    return e
}
let Door = function(roomA, roomB){
    
    let door = new Entity('door', 'a door', ()=>"a " +door.state + " door")
    door.state = 'closed';
    let open = new Transition( new AP("open door"), function(player, params) {player.output += ' You open the door.'; door.state = 'open' })
    let close = new Transition( new AP("close door"), function(player, params) {player.output += ' You close the door.'; door.state = 'closed'})
    let use = new Transition( new AP("use door"), function(player, params) {

        if(getRoom(player.currentRoom) == allRooms[roomA])
            var target = roomB
        else if(getRoom(player.currentRoom) == allRooms[roomB]){
            var target = roomA
        } else {
            player.output += 'Error: The door connects non-places (' + roomA + '<=>' +roomB +') You are in ' + getRoom(player.currentRoom);
            return;
        }


        if(door.state=='closed'){
            player.output += ' You open the door and step into ' +target + '. You close the door behind yourself. '
        } else {
            player.output += 'You step through the open door into ' + target +'. ';
        }
        player.output += 'Ahead of you is ' + allRooms[target].describeDirection(player.facing)        
        player.currentRoom = target
        if(!player.roomdata[target]){
            player.roomdata[target]={}
            player.roomdata[target].timesVisited=0
        } 
        player.roomdata[target].timesVisited += 1
        
    })
    door.transitions = [open, close, use]
    return door;
}


let allQuests = {
    q_hallway : new EnterRoomFirstTimeQuest('hallway', 'You enter the small hallway for the first time') 
}

let getRoom = function(name){
    let room = allRooms[name]
    
    return room
}


let createPlayer = function(){
    let player = {}
    player.input = ''
    player.output = ''
    player.broadcast = ''
    player.facing = 'north'
    player.currentRoom ='office'
    player.quests = [{
        id: 'q_hallway',
        state: {resolved: false}
        }
    ]
    player.completedQuests = [
        
    ]
    player.roomdata = {}
    return player
}

let applyInput = function(player, input){
    //console.log(player)
    let caught = false
    console.log('checking room (look, turn)')
    let room = getRoom(player.currentRoom)
    let matchingTransitions  = room.getTransitions(player.facing)
        //.map(transition => {console.log('available transition: ' +transition.parser.match().pattern); return transition})
        .filter((transition)=>transition.parser.match(input).matching)
    
    matchingTransitions    
        //.map(x=>{console.log('matching transition: ' + x.parser.match().pattern); return x})
        .map(transition => {return transition.action(player, transition.parser.match(input).params)})
    if(matchingTransitions.length>0){
        caught = true;
    }
    
    console.log('checking entities')
    //try entities
    let entities = room.getEntitiesInDirection(player.facing)
    console.log('in reach:' , entities)
    entities = entities.filter((e)=>e.isApplicable(input))
    console.log('applicable',entities)
    if(entities.length>0){
        entities.map(e => e.applyInput(player,input))
        caught = true;
    }
    
    
    //seed quests with player data
    player.quests.map(pq=>{
        allQuests[pq.id].state = pq.state; return allQuests[pq.id] 
    })
    //filter triggered actions
    .filter(q=>q.trigger(player))
    //run actions
    .map(x=>{player.output +='\n QUEST: '; x.action(player); return x})    
    //remove completed quest from player
    player.quests = player.quests.reduce((acc, curr)=>{
        if (curr.state.resolved==true){
            player.completedQuests.push(curr)
            return acc
        } else return acc.concat(curr) },[])
    
    if(!caught){
        player.output += 'No applicable command'
    }

}

module.exports.createPlayer = createPlayer
module.exports.applyInput = applyInput
module.exports.Room = Room
module.exports.Entity = Entity
module.exports.Transition = Transition
module.exports.Door = Door

//API
module.exports.API = {
    getRoom: getRoom
}

module.exports.init = function(rooms){
    allRooms = rooms
}






module.exports.turn = function(facing, dir){
    if(dir == 'left'){
        if(facing=='north')return 'west';
        if(facing=='east')return 'north';
        if(facing=='south')return 'east';
        if(facing=='west')return 'south';  
    } else if (dir == 'right'){
        if(facing=='north')return 'east';
        if(facing=='east')return 'south';
        if(facing=='south')return 'west';
        if(facing=='west')return 'north';         
        
    } else if(dir == 'around' || dir == 'back'){
        if(facing=='north')return 'south';
        if(facing=='east')return 'west';
        if(facing=='south')return 'north';
        if(facing=='west')return 'east';                         
    } else {
        return null
        
    } 
}
module.exports.getDirectionId = function(dir){
   switch(dir){
       case 'north': return 0;
       case 'east': return 1;
       case 'south': return 2;
       case 'west': return 3;
           
   }     
}

module.exports.makeReadableList = function(array){    
    return array.reduce((acc, curr, idx) => {
        if(idx == 0){
            return curr
        }
        if(idx == array.length-1){
            return acc + ' and ' + curr
        } else {
            return acc + ', ' +curr
        }
    },'')
}

module.exports.createWorldFromData = function(data){
    
    
    
    
    
}



var express = require('express')
var session = require('express-session');
var auth = require('./authorization.js')

var app = express()


const util = require('util')

const PORT = process.env.PORT || 5000
const DATABASE = process.env.DATABASE_URL
const SESSION_SECRET = process.env.SESSION_SECRET
const LOGIN_REQUIRED = false;

const VHOST = "@oa$"

var crypto = require('crypto')

var tte = require('./bin/tinytextengine.js'); 
var data = require('./bin/roomdata.js'); 
var roomutils = require('./bin/roomutils.js'); 
tte.init(data.getRooms())






app.use(express.static('public'))
app.use(session({ secret: SESSION_SECRET, cookie: { maxAge: 24 * 3600 * 1000 }}));

app.use(auth.doLogin)

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {   
  res.sendfile('/index.html');
})


app.get('/input', function (req, res) {
    //console.log("got: " + req.query.msg );
   

    let player = req.session.user.player;    
    player.output = ''
    player.broadcast = ''
    tte.applyInput(player, req.query.msg);
    
    let formatted = {
        text: player.output,
        prompt: '>',//req.session.user.auth.name + VHOST
        //speed: 5,
        //color: 'rgb(255,0,0)',
        //effect: 'super-gay'
    }
    res.send(formatted);  
    
        if(player.broadcast)
        io.emit('chat message', player.broadcast);

})


//app.listen(PORT, () => console.log('Listening on port ' +PORT))


var server = require('http').createServer(app);
var io = require('socket.io')(server);
// Add a connect listener
io.on('connection', function(socket) {

    console.log('Client connected.');

    // Disconnect listener
    socket.on('disconnect', function() {
        console.log('Client disconnected.');
    });
});
server.listen(PORT);

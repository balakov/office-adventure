#Office-adventure
Party like it's 1988.

run 'node offlinetest.js' for a singleplayer local command line client without login/logout/register/save. For test purposes.  
run 'node index.js' to host a multiplayer web server. Needs the following environment variables set:    

* DATABASE_URL  
* SESSION_SECRET  
* PORT (optional)   